import React from 'react';
import {StyleSheet, TextInput, View, Image, Text, TouchableOpacity, Alert} from 'react-native';
import {tryRegisterUser} from "../Api/TuCarburWebApi";
import {connect} from "react-redux";

class Enregistrement extends React.Component {

    constructor(props) {
        super(props);
    }

    state = {
        mdp1: '',
        mdp2: '',
        login: ''
    }

    _toggleLogin(actionType, userInfo, isLogin) {
        const action = {type: actionType, value: userInfo, isLogin: isLogin}

        this.props.dispatch(action);
    }

    async _EnregistrementUser(login, mdp1, mdp2) {
        if (mdp1 === mdp2) {
            let userInfo = await tryRegisterUser(login, mdp1)

            console.log(userInfo);

            if (userInfo.isLogin) {
                this._toggleLogin("TOGGLE_LOGIN", userInfo, userInfo.isLogin)
                this.props.navigation.navigate('HomeUserTabNavigator', { 'userInfo': userInfo})
            } else {
                Alert.alert("Erreur d'enregistrement", 'Les informations que vous venez de saisir sont incorrect.')
            }
        } else {
            alert("Merci d'utiliser 2 fois le même mot de passe")
        }
    }

    handleMdp1 = (text) => {
        this.setState({ mdp1: text })
    }
    handleMdp2 = (text) => {
        this.setState({ mdp2: text })
    }
    handleLogin = (text) => {
        this.setState({ login: text })
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../Images/tuCarburLogo.jpg')} style={styles.icon} />
                <Text style={styles.titlePage}>Inscription sur l'application TuCarbur</Text>
                <TextInput style={styles.input} placeholder='Entrer votre identifiant' onChangeText = {this.handleLogin}/>
                <TextInput style={styles.input} placeholder='Entrer votre mot de passe' onChangeText = {this.handleMdp1}/>
                <TextInput style={styles.input} placeholder='Confirmer votre mot de passe' onChangeText = {this.handleMdp2}/>
                <TouchableOpacity
                    style={styles.btn}
                    onPress={() => this._EnregistrementUser(this.state.login, this.state.mdp1, this.state.mdp2)}
                    underlayColor='#fff'>
                    <Text style={styles.btnText}>Inscription</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 40,
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    titlePage: {
        marginBottom: 30,
        marginTop: 30,
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        family: 'Arial'
    },
    btn: {
        backgroundColor: '#273508',
        paddingTop:10,
        paddingBottom:10,
        borderRadius: 10,
        width: 180,
    },
    btnText: {
        color:'#fff',
        textAlign:'center',
        family: 'Arial',
        fontSize: 16
    },
    icon: {
        width: 300,
        height: 300
    },
    input: {
        width: 300,
        padding: 5,
        marginBottom: 20,
        borderStartWidth : 2,
        borderEndWidth : 2,
        borderTopWidth : 2,
        borderLeftWidth: 2,
        borderRightWidth: 2,
        borderBottomWidth : 2
    }
});

const mapStateToProps = (state) => {
    return {
        userInformation : state.userInformation
    }}

export default connect(mapStateToProps)(Enregistrement)