import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import StationDetail from "./StationDetail";

class StationItem extends React.Component {

    constructor(props) {
        super(props);
    }
    _displayDetailForStation = (idStation) => {
        this.props.navigation.navigate('StationDetail', {idStation: idStation});
    }

    render() {
        const {station} = this.props
        return (
            <TouchableOpacity style={styles.main_container} onPress={() => this._displayDetailForStation(station.id)}>
                <View style={styles.content_container}>

                    <View style={styles.header_container}>
                        <Text style={styles.marque_text}>Station {station.marque}</Text>
                        <Text style={styles.adresse_postale_text}>{station.adresse_postale}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        height: 190,
        flexDirection: 'row'
    },
    image: {
        width: 120,
        height: 180,
        margin: 5,
        backgroundColor: 'gray'
    },
    content_container: {
        flex: 1,
        margin: 5,
        marginTop: 40
    },
    header_container: {
        flex: 3,
    },
    marque_text: {
        fontWeight: 'bold',
        fontSize: 26,
        color: 'black',
        marginBottom: 5,
        textAlign: 'center'
    },
    adresse_postale_text: {
        fontSize: 14,
        flex: 1,
        flexWrap: 'wrap',
        paddingRight: 5,
        textAlign: 'center'
    },
})

export default StationItem