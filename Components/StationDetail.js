import React from 'react'
import { StyleSheet, View, Text, Image , ScrollView, TouchableOpacity, FlatList} from 'react-native'
import { connect } from 'react-redux'
import {getStationDetailById, miseAJourStock, ModifFavoris} from "../Api/TuCarburWebApi";
import {Picker} from "@react-native-community/picker";

class StationDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            station: '',
            carburants:''
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.getStationDetail(this.props.navigation.state.params.idStation)
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    async getStationDetail(idStation) {
        let resultStationDetail = await getStationDetailById(idStation);

        this.setState({station: resultStationDetail});

        this.setState({carburants: resultStationDetail.stationCarburants})
    }

    handleSubmit(event) {
        miseAJourStock(this.state.value.id, this.props.navigation.state.params.idStation, this.state.value.price)
    }

    async _toggleFavorite(userId, stationId) {
        if (this.props.userInformation[2] !== stationId) {
            let responseFavorite = await ModifFavoris(userId, stationId)
        }
    }


    _displayFavoriteImage() {
        let source;

        if (this.props.navigation.state.params.idStation === this.props.userInformation[2]) {
            source = require('../Images/favorite.png');
        } else {
            source = require('../Images/non_favorite.png');
        }

        return(<Image style={styles.favorite_image} source={source} />)
    }

    _displayRenderItem(item) {
        console.log(item);
        return (
        <View>
            <Text style={styles.text}>Marque : {item.marque}</Text>
            <Text style={styles.text}>Requette : {item.adresse_postale}</Text>
            <Text style={styles.text}>{item.dateMiseAjour}</Text>

            <form onSubmit={this.handleSubmit}>
                <label>Prix par litre: <input type="text" value={item.s} onChange={this.handleChange} /></label>
                <input type="submit" value="Submit" />
            </form>
        </View>
    )
    }

    _DisplayDetailForStationWithId(){
        return (
            <View>
                <Text>Station Detail</Text>

                <TouchableOpacity onPress={() => this._toggleFavorite(this.props.userInformation[0], this.props.navigation.state.params.idStation)}>
                    {this._displayFavoriteImage()}
                </TouchableOpacity>

                <Text style={styles.text}>Marque : {this.state.station.marque}</Text>
                <Text style={styles.text}>Adresse : {this.state.station.adresse_postale}</Text>

                <Text style={styles.textCarburant}>Carburant disponible</Text>
                <FlatList
                    data={this.state.carburants}
                    renderItem={({ item, index, separators }) => (
                            <View style={{ backgroundColor: 'white' }}>
                                <Text>{item.nom} : {item.price}€ / litre</Text>
                            </View>
                    )}
                />
            </View>
        )
    }

    render() {
        return (
            <View style={styles.main_container}>
                {this._DisplayDetailForStationWithId()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white'
    },
    image: {
        width: 120,
        height: 180,
        margin: 5,
        backgroundColor: 'gray'
    },
    favorite_image: {
        width: 40,
        height: 40
    },
    text: {
        fontWeight: 'bold',
        fontSize: 20,
        flex: 1,
        flexWrap: 'wrap',
        paddingRight: 5
    },
    textCarburant: {
        marginTop:10,
        marginBottom: 10,
        fontWeight: 'bold'
    }
})


const mapStateToProps = (state) => {
    return {
        userInformation : state.userInformation
    }}

export default connect(mapStateToProps)(StationDetail)
