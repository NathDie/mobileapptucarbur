import React, { Component } from 'react';
import {StyleSheet, Image, View, Text, TouchableOpacity, TextInput, Alert} from 'react-native';
import {TryChangeUserInfo} from "../Api/TuCarburWebApi";
import {connect} from "react-redux";

class ProfilUser extends Component {

    constructor(props) {
        super(props);
    }

    handleMdp1 = (text) => {
        this.setState({ mdp1: text })
    }
    handleMdp2 = (text) => {
        this.setState({ mdp2: text })
    }
    handleLogin = (text) => {
        this.setState({ login: text })
    }

    async _ChangementMdp(login, mdp1, mdp2) {
        if (mdp1 === mdp2) {
            let userInfo = await TryChangeUserInfo(login, mdp1, this.props.userInformation[O])

            console.log(userInfo);

            if (userInfo.isLogin) {
                this.props.navigation.navigate('HomeTabNavigator')
            } else {
                Alert.alert("Erreur d'enregistrement", 'Les informations que vous venez de saisir sont incorrect.')
            }
        } else {
            alert("Merci d'utiliser 2 fois le même mot de passe")
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../Images/tuCarburLogo.jpg')} style={styles.icon} />
                <Text style={styles.titlePage}>Tu as besoin de changer ton mot de passe ?</Text>
                <TextInput style={styles.input} placeholder='Entrer votre identifiant' onChangeText = {this.handleLogin}/>
                <TextInput style={styles.input} placeholder='Entrer votre mot de passe' onChangeText = {this.handleMdp1}/>
                <TextInput style={styles.input} placeholder='Confirmer votre mot de passe' onChangeText = {this.handleMdp2}/>
                <TouchableOpacity
                    style={styles.btn}
                    onPress={() => this._ChangementMdp(this.state.login, this.state.mdp1, this.state.mdp2)}
                    underlayColor='#fff'>
                    <Text style={styles.btnText}>Inscription</Text>
                </TouchableOpacity>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        marginTop: 5,
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    titlePage: {
        marginBottom: 50,
        marginTop: 30,
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        family: 'Arial'
    },
    btnConnection: {
        marginBottom: 50,
        backgroundColor: '#0FA1CF',
        paddingTop:10,
        paddingBottom:10,
        borderRadius: 10,
        width: 130,
    },
    btnChangerMdp: {
        marginBottom: 50,
        backgroundColor: '#273508',
        paddingTop:10,
        paddingBottom:10,
        borderRadius: 10,
        width: 150,
    },
    btnEnregistrement: {
        marginBottom: 50,
        backgroundColor: '#DA5409',
        paddingTop:10,
        paddingBottom:10,
        borderRadius: 10,
        width: 130,
    },
    btn: {
        backgroundColor: '#273508',
        paddingTop:10,
        paddingBottom:10,
        borderRadius: 10,
        width: 180,
    },
    btnText: {
        color:'#fff',
        textAlign:'center',
        family: 'Arial',
        fontSize: 16
    },
    icon: {
        width: 300,
        height: 300
    },
    input: {
        width: 300,
        padding: 5,
        marginBottom: 20,
        borderStartWidth : 2,
        borderEndWidth : 2,
        borderTopWidth : 2,
        borderLeftWidth: 2,
        borderRightWidth: 2,
        borderBottomWidth : 2
    }
});


const mapStateToProps = (state) => {
    return {
        userInformation : state.userInformation
    }}

export default connect(mapStateToProps)(ProfilUser)