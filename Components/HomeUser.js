import React from 'react';
import {Image, StyleSheet, Text, View} from "react-native";
import { connect } from 'react-redux';
import { getStationDetailById } from "../Api/TuCarburWebApi";

class HomeUser extends React.Component {
    constructor(props) {
        super(props);

        this.state = { station : undefined }

        this._getFavoriteStation();
    }


    async _getFavoriteStation() {
        if (this.props.userInformation[2] !==  '00000000-0000-0000-0000-000000000000') {
            let resultFavorite = await getStationDetailById(this.props.userInformation[2])

            this.setState({station: resultFavorite})
        }
    }

    _renderStationFavorite() {
        if(this.state.station !== undefined) {
            return (<View style={ styles.containerStationFavorite }>
                <Text style={ styles.text }> Votre station favorite :</Text>
                <Text style={styles.textMarque}>Marque : {this.state.station.marque}</Text>
                <Text style={styles.textMarque}>Adresse : {this.state.station.adresse_postale}</Text>
            </View> )
        } else {
            return (
                <View style={ styles.containerStationFavorite }>
                    <Text style={ styles.text }>Vous avez pas de station favorite</Text>
                </View>
            )
        }
    }
    render() {
        return (
            <View style={ styles.main_container }>
                <Image source={require('../Images/tuCarburLogo.jpg')} style={styles.icon} />
                <Text style={ styles.textHome }>Bonjour, <Text  style={styles.textUser}>{this.props.userInformation[1]}</Text>.</Text>
                {this._renderStationFavorite()}
            </View>



        )
    }
}


const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white',
        marginTop: 15
    },
    containerStationFavorite: {
        marginTop: 15
    },
    text: {
        fontWeight: 'bold',
        fontSize: 20,
        marginTop: 10,
        marginBottom: 20,
    },
    textHome: {
        fontSize: 20,
        marginTop: 45,
        marginBottom: 20,
    },
    textUser: {
        fontWeight: 'bold',
    },
    textMarque: {
      textAlign:'center'
    },
    icon: {
        width: 300,
        height: 300
    },
})


const mapStateToProps = (state) => {
    return {
        userInformation : state.userInformation
    }}

//export default HomeUser
export default connect(mapStateToProps)(HomeUser)