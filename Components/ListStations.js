import React from "react";
import {StyleSheet, FlatList, ScrollView} from 'react-native';

import {getStations, getCarburants, getStationsByCarburant} from '../api/TuCarburWebApi'
import StationItem from "./StationItem";
import {Picker} from "@react-native-community/picker";

const styles = StyleSheet.create({
    margin_top: {
        marginTop: 20
    },
    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

class ListStations extends React.Component
{
    constructor(props) {
        super(props);

        this.state = { stations : this._loadStations(), carburants: [], selectedCarburant: '' }

        this._loadCarburants();
    }

    _displayDetailForStation = (idStation) => {
        this.props.navigation.navigate('StationDetail', {idStation: idStation});
    }


    async _loadStations() {
        let resultStations = await getStations();

        this.setState({stations: resultStations})
    }

    async _loadStationsWithOneCarburant(carburantId) {

        this.setState({selectedCarburant:carburantId})

        this.setState({stations: []})

        let resultStationsWithCarburant = await getStationsByCarburant(carburantId);

        this.setState({stations: resultStationsWithCarburant})

        this.setState({refresh: true})
    }

    async _loadCarburants() {
        let resultCarburants = await getCarburants()

        this.setState({carburants: resultCarburants})
    }

    render() {
        return (
            <ScrollView style={styles.margin_top}>
                <Picker selectedValue={this.state.selectedCarburant} onValueChange={(value)=> this._loadStationsWithOneCarburant(value)}>
                    {this.state.carburants.map((item, key)=>(
                        <Picker.Item label={item.nom} value={item.id} key={key} />)
                    )}>

                </Picker>

                <FlatList
                    extraData={this.state}
                    data={this.state.stations}
                    keyExtractor={(item) =>  item.id.toString()  }
                    renderItem={( {item} ) => <StationItem station={item} navigation={this.props.navigation}/> }
                />
            </ScrollView>
        );
    }

}

export default ListStations