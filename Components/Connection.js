import React from 'react';
import {StyleSheet, TextInput, View, TouchableOpacity, Image, Text, Alert} from 'react-native';
import {tryLoginUser} from "../Api/TuCarburWebApi";
import { connect } from 'react-redux'


class Connection extends React.Component {

    constructor(props) {
        super(props);
    }

    _toggleLogin(actionType, userInfo, isLogin) {
        const action = {type: actionType, value: userInfo, isLogin: isLogin}

        this.props.dispatch(action);
    }

    state = {
        login: '',
        mdp: ''
    }

    async _loginUser(login, mdp) {
       let userInfo = await tryLoginUser(login, mdp);

       if (userInfo.isLogin) {
           this._toggleLogin("TOGGLE_LOGIN", userInfo, userInfo.isLogin)
           this.props.navigation.navigate('HomeUserTabNavigator', { 'userInfo': userInfo})
       } else {
           Alert.alert("Erreur d'authentification", 'Les informations que vous venez de saisir sont incorrect.')
       }
    }

    handleLogin = (text) => {
        this.setState({ login: text })
    }
    handleMdp = (text) => {
        this.setState({ mdp: text })
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../Images/tuCarburLogo.jpg')} style={styles.icon} />
                <Text style={styles.titlePage}>Identifie toi afin d'accéder sur l'application TuCarbur</Text>
                <TextInput style={styles.input} placeholder='Entrer votre identifiant' onChangeText = {this.handleLogin}/>
                <TextInput style={styles.input} placeholder='Entrer votre password' onChangeText = {this.handleMdp}/>
                <TouchableOpacity
                    style={styles.btn}
                    onPress={() => this._loginUser(this.state.login, this.state.mdp)}
                    underlayColor='#fff'>
                    <Text style={styles.btnText}>Connection</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 40,
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    titlePage: {
        marginBottom: 30,
        marginTop: 30,
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        family: 'Arial'
    },
    btn: {
        backgroundColor: '#0FA1CF',
        paddingTop:10,
        paddingBottom:10,
        borderRadius: 10,
        width: 100,
    },
    btnText: {
        color:'#fff',
        textAlign:'center',
        family: 'Arial',
        fontSize: 16
    },
    icon: {
        width: 300,
        height: 300
    },
    input: {
        width: 300,
        padding: 5,
        marginBottom: 20,
        borderStartWidth : 2,
        borderEndWidth : 2,
        borderTopWidth : 2,
        borderLeftWidth: 2,
        borderRightWidth: 2,
        borderBottomWidth : 2
    }
});


const mapStateToProps = (state) => {
    return {
    userInformation : state.userInformation
}}

export default connect(mapStateToProps)(Connection)