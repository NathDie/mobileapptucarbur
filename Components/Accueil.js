import React, { Component } from 'react';
import {StyleSheet, Image, View, Text, TouchableOpacity} from 'react-native';

class Accueil extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../Images/tuCarburLogo.jpg')} style={styles.icon} />
                <Text style={styles.titlePage}>Bienvenue sur l'application TuCarbur</Text>
                <Text style={styles.presentationText}>Tu a besoin de trouver le carburant le moins cher et le plus proche de votre position ? Alors n'hésite pas à t'inscrire sur l'application</Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        marginTop: 5,
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon: {
        width: 300,
        height: 300
    },
    titlePage: {
        marginBottom: 50,
        marginTop: 30,
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        fontfamily: 'Arial'
    },
    presentationText: {
        textAlign: 'center',
        padding: 10,
        fontSize: 16,
    }
});


export default Accueil