const initialUserInformation = { userInformation: []}

function userInformations(state = initialUserInformation, action){
    let nextState;

    switch (action.type) {
        case 'TOGGLE_LOGIN':
            let userInfo = [action.value.id, action.value.login, action.value.stationId];

            nextState = {
                ...state,
                userInformation: userInfo
            }

            return nextState || state;

        case 'TOGGLE_LOGOUT':
            nextState = {
                ...state,
                userInformation: []
            }

            return nextState || state;

        default:
            return state;
    }
}

export default userInformations