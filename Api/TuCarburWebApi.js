const baseUrl = "https://localhost:5001/api";
let userInfo = '';
let isRegister = false;
let isChange = false;
let resultStation;
let resultDetailStation;
let resultStock = false;
let resultStationByCarburant;

export async function tryLoginUser(login, mdp) {
    const url = baseUrl + '/utilisateur/connection';

    let responseLogin = await fetch(url, {
        method: 'POST',
        accept: 'application/json',
        body: JSON.stringify({
            Login: login,
            Mdp: mdp
        }),
        headers: {
            'Accept': "application/json, text/plain, */*",
            'Content-Type': "application/json;charset=utf-8",
        }
    });

    if (responseLogin.ok) {
        let json = await responseLogin.json();

        if (json !== null) {
            json.isLogin = true;

            userInfo = json
        }
    } else {
        userInfo = {"isLogin": false};
    }

    return userInfo;
}

export async function tryRegisterUser(login, mdp) {
    const url = baseUrl + '/utilisateur/enregistrement';

    let responseRegister = await fetch(url, {
        method: 'POST',
        accept: 'application/json',
        body: JSON.stringify({
            Login: login,
            Mdp: mdp
        }),
        headers: {
            'Accept': "application/json, text/plain, */*",
            'Content-Type': "application/json;charset=utf-8",
        }
    });

    console.log(responseRegister)

    if (responseRegister.ok) {
        let json = await responseRegister.json();

        if (json !== null) {
            json.isLogin = true;

            userInfo = json

        }
    } else {
        userInfo = {"isLogin": false};
    }

    return userInfo;
}


export async function TryChangeUserPasswird(login, mdp) {
    const url = baseUrl + '/utilisateur/changepassword';

    let responseUpdate = await fetch(url, {
        method: 'PUT',
        accept: 'application/json',
        body: JSON.stringify({
            Login: login,
            Mdp: mdp
        }),
        headers: {
            'Accept': "application/json, text/plain, */*",
            'Content-Type': "application/json;charset=utf-8",
        }
    });

    console.log(responseUpdate)

    if (responseUpdate.ok) {
        let json = await responseUpdate.json();

        if (json !== null) {
            json.isLogin = true;

            userInfo = json

        }
    } else {
        userInfo = {"isLogin": false};
    }

    return userInfo;
}

export async function TryChangeUserInfo(login, mdp, userId) {
    const url = baseUrl + '/utilisateur/changeuser/' + userId;

    let responseUpdate = await fetch(url, {
        method: 'PUT',
        accept: 'application/json',
        body: JSON.stringify({
            Login: login,
            Mdp: mdp
        }),
        headers: {
            'Accept': "application/json, text/plain, */*",
            'Content-Type': "application/json;charset=utf-8",
        }
    });

    console.log(responseUpdate)

    if (responseUpdate.ok) {
        let json = await responseUpdate.json();

        if (json !== null) {
            json.isLogin = true;

            userInfo = json

        }
    } else {
        userInfo = {"isLogin": false};
    }

    return userInfo;
}


export async function getStations() {
    const url = baseUrl + '/station';

    let response = await fetch(url, { method: 'GET'});

    if (response.ok) {
        let json = await response.json();

        if (json !== null) {
            resultStation = json;
        }
    }

    return resultStation;
}

export async function getStationsByCarburant(carburantId) {
    const url = baseUrl + '/station/bycarburant/' + carburantId;

    let response = await fetch(url, { method: 'GET'});

    if (response.ok) {
        let json = await response.json();

        if (json !== null) {
            resultStationByCarburant = json;
        }
    }

    return resultStationByCarburant;
}

export async function getCarburants() {
    const url = baseUrl + '/carburant';

    let response = await fetch(url, { method: 'GET'});

    if (response.ok) {
        let json = await response.json();

        if (json !== null) {
            resultStation = json;
        }
    }

    return resultStation;
}

export async function ModifFavoris(utilisateurId, stationId) {
    const url = baseUrl + '/utilisateur/modiffav';

    let response = await fetch(url, {
        method: 'PUT',
        accept: 'application/json',
        body: JSON.stringify({
            UtilisateurId: utilisateurId,
            StationId: stationId
        }),
        headers: {
            'Accept': "application/json, text/plain, /",
            'Content-Type': "application/json;charset=utf-8",
        }
    });

    if (response.ok) {
        let json = await response.json();

        if (json !== null) {
            resultStation = json;
        }
    }

    return resultStation;
}

export async function miseAJourStock(stationid, carburantid, price) {
    const url = baseUrl + '/enstock/majstock';

    let response = await fetch(url, {
        method: 'PUT',
        accept: 'application/json',
        body: JSON.stringify({
            StationId: utilisateurId,
            CarburantId: carburantId,
            PrixParLitre: price
        }),
        headers: {
            'Accept': "application/json, text/plain, /",
            'Content-Type': "application/json;charset=utf-8",
        }
    });

    if (response.ok) {
        let json = await response.json();

        if (json !== null) {
            resultStock = json;


        }
    }

    return resultStock;
}

export async function getStationDetailById(stationid) {
    const url = baseUrl + '/station/details/' + stationid;

    let response = await fetch(url, {
        method: 'GET',
        accept: 'application/json',
        headers: {
            'Accept': "application/json, text/plain, /",
            'Content-Type': "application/json;charset=utf-8",
        }
    });

    if (response.ok) {
        let json = await response.json();

        if (json !== null) {
            resultDetailStation = json;
        }
    }

    return resultDetailStation;
}