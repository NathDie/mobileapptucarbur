import React from 'react';
import { StyleSheet, Text, Image, View } from 'react-native';
import Accueil from "./Components/Accueil";
import {Provider} from "react-redux";
import Store from './Store/configureStore'
import {NavigationContainer} from "@react-navigation/native";
import Navigation from "./Navigation/Navigation";

export default function App() {
  return (
    <Provider store={Store}>
        <NavigationContainer>
            <Navigation />
        </NavigationContainer>
    </Provider>
  );

}
