import React from 'react';

import HomeUser from "../Components/HomeUser";
import ListStations from "../Components/ListStations";
import Connection from "../Components/Connection";
import Enregistrement from "../Components/Enregistrement";
import StationDetail from "../Components/StationDetail";
import Accueil from '../Components/Accueil';
import ChangeMdp from "../Components/ChangeMdp";
import ProfilUser from "../Components/ProfilUser";
import {createBottomTabNavigator } from 'react-navigation-tabs'
import {createAppContainer, createSwitchNavigator} from "react-navigation";
import createStackNavigator from "react-native-screens/createNativeStackNavigator";
import {StyleSheet, Image} from 'react-native';

const ListStationStackNavigator = createStackNavigator({
    ListStations: {
        screen: ListStations,
    },
    StationDetail: {
        screen: StationDetail,
    },
})

const HomeTabNavigator = createBottomTabNavigator({
        Accueil: {
            screen: Accueil,
            navigationOptions: {
                tabBarIcon: () => {
                    return <Image source={require('../Images/home.jpg')}
                                  style={styles.icon} />
                }
            }
        },
        Connection: {
            screen: Connection,
            navigationOptions: {
                tabBarIcon: () => {
                    return <Image source={require('../Images/login.png')}
                                  style={styles.icon} />
                }
            }
        },
        Enregistrement: {
            screen: Enregistrement,
            navigationOptions: {
                tabBarIcon: () => {
                    return <Image source={require('../Images/inscription.png')}
                                  style={styles.icon} />
                }
            }
        },
        ChangeMdp: {
            screen: ChangeMdp,
            navigationOptions: {
                tabBarIcon: () => {
                    return <Image source={require('../Images/password.png')}
                                  style={styles.icon} />
                }
            }
        },
    },
    {
        tabBarOptions: {
            activeBackgroundColor: '#DDDDDD',
            inactiveBackgroundColor: "#FFFFFF",
            showLabel: false,
            showIcon: true
        }
    })

const HomeUserTabNavigator = createBottomTabNavigator({
        HomeUser: {
            screen: HomeUser,
            navigationOptions: {
                tabBarIcon: () => {
                    return <Image source={require('../Images/home.jpg')}
                                  style={styles.icon} />
                }
            }
        },
        ListStationStack: {
            screen : ListStationStackNavigator,
            navigationOptions: {
                tabBarIcon: () => {
                    return <Image source={require('../Images/station.png')}
                                  style={styles.icon} />
                }
            }
        },
        ProfilUser: {
            screen : ProfilUser,
            navigationOptions: {
                tabBarIcon: () => {
                    return <Image source={require('../Images/profil.png')}
                                  style={styles.icon} />
                }
            }
        },
    },
    {
        tabBarOptions: {
            activeBackgroundColor: '#DDDDDD',
            inactiveBackgroundColor: "#FFFFFF",
            showLabel: false,
            showIcon: true
        }
    })

const styles = StyleSheet.create({
    icon: {
        width: 30,
        height: 30
    }
})


export default createAppContainer(createSwitchNavigator(
    {
        HomeTabNavigator: HomeTabNavigator,
        HomeUserTabNavigator: HomeUserTabNavigator,
    },
    {
        initialRouteName: 'HomeTabNavigator',
    }
));